# Details
ATOM is meeting on Tuesday, 8th May, 6:30pm, at Galvanize!

Join us for a highly participative discussion !

This time, our own Matt Drury will lead a discussion on :

# How We Use Our Models: To Explain or Predict?

Statistical modeling is a tool we all love, that can be used to solve problems in many different domains from many different angles, though those angles are too rarely differentiated in discussion and literature. In this session we will discuss two uses of statistical models, and the different concerns and techniques we must adopt to work in these domains. Our guide will be the paper To Explain or Predict, by Galit Shmueli.

I will be leading this session as a topic discussion, with my role to seed questions and thoughts. It is important that all attendees read the paper, and develop some opinions and questions to discuss with the group.

Supplemental Materials:

Shmueli has a short talk on her paper that is a fun watch: https://www.youtube.com/watch?v=vWH_HNfQVRI

Explain or Predict paper:
https://www.stat.berkeley.edu/~aldous/157/Papers/shmueli.pdf

Another important and related dichotomy we may find time to discuss is between classical data models and more modern algorithmic models. This is covered in the classic (as in, everyone should read this one) "Two Cultures" paper: Statistical Modeling: The Two Cultures by Leo Breiman (of Random Forest fame).

About ATOM:
Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !